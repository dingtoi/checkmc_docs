import '../../public/uikit.min.css';
import Link from 'next/link';

function HomePage() {
  return (
    <div className="uk-section">
      <div className="uk-container">
        <ul className="uk-list uk-list-divider">
          <li>
            <Link href="/">
              <button className="uk-button uk-button-link">Back</button>
            </Link>
          </li>
          <li>
            <dl className="uk-description-list uk-description-list-divider">
              <dt>Step 1</dt>
              <dd>Go to url and download IOS build: <a href="https://ioair.link/8c4qn3">https://ioair.link/8c4qn3</a></dd>
              <dt>Step 2</dt>
              <dd>For installation instructions, click on <a href="ios.pdf">PDF Installation Manual</a>.</dd>
              <dt>Worflow</dt>
              <dd>Download Worflow Here: <a href="workflow-ios.pdf">Worflow For Scanner IOS</a></dd>
              {/*<dt>Additional</dt>
              <dd>Use Dingtoi MC, Tutorial for using: <a href="https://buivuong748273.invisionapp.com/public/share/6H13O5KA42#/screens/477374250">Click Here</a></dd>*/}
            </dl>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default HomePage;