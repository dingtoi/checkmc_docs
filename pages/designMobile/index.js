import Head from "next/head";
import Link from "next/link";

function Design() {
  return (
    <div style={{ paddingBottom: "20px" }}>
      <Head>
        <title>Design</title>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css"
        />
        <script
          src="https://code.jquery.com/jquery-3.5.1.min.js"
          integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
          crossOrigin="anonymous"
        ></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
      </Head>
      <div className="uk-grid uk-container" style={{ margin: "20px" }}>
        <h2>For Recipient</h2>
      </div>
      <div className="uk-grid uk-child-width-1-4">
        <div>
          <Link href="designMobile/login">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/login.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Sign In Page</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/signUp">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/signup.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Sign Up Page</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/forgotPass">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/forgot_password.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Forgot Password Page</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
      </div>
      <div className="uk-grid uk-container" style={{ margin: "20px" }}>
        <h2>For Originator</h2>
      </div>
      <div className="uk-grid uk-child-width-1-4">
        <div>
          <Link href="designMobile/deviceAccountList">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/device_list.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Device List</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>


        <div>
          <Link href="designMobile/deviceAccountAdd">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/Device_add.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Device Add</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/deviceAccountDetail">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/Device_detail.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">Device Detail - Status Available</p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/deviceAccountDelete">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/device_detail_delete.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">
                Device Detail Delete - Status Available
              </p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/deviceAccountEdit">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/Device_edit.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">
                Device Detail Edit - Status Available
              </p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/devicePostSellExchange">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/device_post_sell_exchange.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">
                Add Device Post - Step 1 - Sell Exchange
              </p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/devicePostInformation">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/device_post_information.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">
                Add Device Post - Step 2 - Device Information
              </p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link href="designMobile/devicePostUploadPhotos">
            <div className="uk-container" style={{textAlign: "center"}}>
              <img
                src="designMobile/device_post_upload_photos.png"
                alt=""
                style={{ height: "200px" }}
              />
              <p className="uk-text-center">
                Add Device Post - Step 1 - Upload Photos
              </p>
              <div
                className="uk-button uk-button-primary uk-button-small"
                style={{ width: "100%" }}
              >
                View
              </div>
            </div>
          </Link>
        </div>

      </div>
      <div className="uk-grid uk-container" style={{ margin: "20px" }}>
          <h2>For Recipient</h2>
      </div>
      <div className="uk-grid uk-child-width-1-4">
          <div>
              <Link href="designMobile/home">
                  <div className="uk-container" style={{ textAlign: "center" }}>
                      <img
                          src="designMobile/home.png"
                          alt=""
                          style={{ height: "200px" }}
                      />
                      <p className="uk-text-center">
                          Home
                      </p>
                      <div
                          className="uk-button uk-button-primary uk-button-small"
                          style={{ width: "100%" }}
                      >
                          View
                      </div>
                  </div>
              </Link>
          </div>
          <div>
              <Link href="designMobile/category">
                  <div className="uk-container" style={{ textAlign: "center" }}>
                      <img
                          src="designMobile/category.png"
                          alt=""
                          style={{ height: "200px" }}
                      />
                      <p className="uk-text-center">
                          Category
                      </p>
                      <div
                          className="uk-button uk-button-primary uk-button-small"
                          style={{ width: "100%" }}
                      >
                          View
                      </div>
                  </div>
              </Link>
          </div>
      </div>
      <a
        href="/"
        className="uk-border-circle"
        style={{
          width: "70px",
          height: "70px",
          backgroundColor: "orange",
          color: "white",
          position: "fixed",
          bottom: 100,
          right: 40,
          textDecoration: "none",
          lineHeight: "70px",
          textAlign: "center",
        }}
      >
        Back
      </a>
    </div>
  );
}

export default Design;
