import "../../public/uikit.min.css";
import Link from "next/link";

function HomePage() {
  return (
    <div className="uk-section">
      <div className="uk-container">
        <ul className="uk-list uk-list-divider">
          <li>
            <Link href="/">
              <button className="uk-button uk-button-link">Back</button>
            </Link>
          </li>
          <li>
            <dl className="uk-description-list uk-description-list-divider">
              <dt>Dingtoi Lite</dt>
              <dd>
                Download the <a href="app-release.apk">Dingtoi APK</a> on your
                mobile to continue
              </dd>
              <dt>Dingtoi Pro</dt>
              <dd>
                Download the <a href="app-release-pro.apk">Dingtoi Pro APK</a>{" "}
                on your mobile to continue
              </dd>
              {/*<dt>Additional</dt>
              <dd>Use Dingtoi MC, Tutorial for using: <a href="https://buivuong748273.invisionapp.com/public/share/NG13NRQ2WK#/screens/477371925">Click Here</a></dd>*/}
            </dl>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default HomePage;
