import '../../public/uikit.min.css';
import Link from 'next/link';

function HomePage() {
  return (
    <div className="uk-section">
      <div className="uk-container">
        <ul className="uk-list uk-list-divider">
          <li>
            <Link href="/">
              <button className="uk-button uk-button-link">Back</button>
            </Link>
          </li>
          <li>
            <dl className="uk-description-list uk-description-list-divider">
              <dt>Base</dt>
              <dd>View <a href="workflow/base.png">Base Workflow</a></dd>
              <dt>User Anonymous</dt>
              <dd>View <a href="workflow/user_anonymous.png">User Anonymous</a></dd>
              <dt>User Free</dt>
              <dd>View <a href="workflow/user_free.png">User Free</a></dd>
              <dt>User Fee</dt>
              <dd>View <a href="workflow/user_fee.png">User Fee</a></dd>
              {/*<dt>Additional</dt>
              <dd>Use Dingtoi MC, Tutorial for using: <a href="https://buivuong748273.invisionapp.com/public/share/NG13NRQ2WK#/screens/477371925">Click Here</a></dd>*/}
            </dl>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default HomePage;