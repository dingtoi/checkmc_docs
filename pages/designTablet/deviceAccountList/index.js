import Head from "next/head";
import Link from "next/link";

function Login() {
  return (
    <div>
      <Head>
        <title>Design</title>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css"
        />
        <script
          src="https://code.jquery.com/jquery-3.5.1.min.js"
          integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
          crossOrigin="anonymous"
        ></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
      </Head>
      <div style={{ width: "768px", margin: "0 auto" }}>
        <img
          src="/designTablet/header_admin.png"
          style={{ width: "100%", height: "auto" }}
        />
        <img
          src="/designTablet/device_list.png"
          style={{ width: "100%", height: "auto" }}
        />
        <img
          src="/designTablet/footer.png"
          style={{ width: "100%", height: "auto" }}
        />
      </div>
      <a
        href="/designTablet"
        className="uk-border-circle"
        style={{
          width: "70px",
          height: "70px",
          backgroundColor: "orange",
          color: "white",
          position: "fixed",
          bottom: 100,
          right: 40,
          textDecoration: "none",
          lineHeight: "70px",
          textAlign: "center",
        }}
      >
        Back
      </a>
    </div>
  );
}

export default Login;
