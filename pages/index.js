import "../public/uikit.min.css";
import Link from "next/link";

function HomePage() {
  return (
    <div className="uk-section">
      <div className="uk-container">
        <ul className="uk-list">
          <li className="uk-margin-medium-bottom">
            {/*<div className="uk-width-1-1 uk-flex uk-flex-center">
              <img src="logo.png" style={{ maxWidth: '170px' }} />
            </div>*/}
          </li>
          <li>
            <Link href="/designDesktop">
              <button className="uk-button uk-button-secondary uk-width-1-1">
                Design On PC/Desktop
              </button>
            </Link>
          </li>
          <li>
            <Link href="/designTablet">
              <button className="uk-button uk-button-secondary uk-width-1-1">
                Design On Tablet
              </button>
            </Link>
          </li>
          <li>
            <Link href="/designMobile">
              <button className="uk-button uk-button-secondary uk-width-1-1">
                Design On Mobile
              </button>
            </Link>
          </li>
          <li>
            <Link href="/android">
              <button className="uk-button uk-button-primary uk-width-1-1">
                ANDROID: How to install
              </button>
            </Link>
          </li>
          <li>
            <Link href="/ios">
              <button className="uk-button uk-button-secondary uk-width-1-1">
                IOS: How to install
              </button>
            </Link>
          </li>
          <li>
            <Link href="/workflow">
              <button className="uk-button uk-button-secondary uk-width-1-1">
                Workflow
              </button>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default HomePage;
